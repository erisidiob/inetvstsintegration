/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.vstsservices;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmara_import.remote.RemoteServiceParameterObject;
import it.dsgroup.inetvsts.InetVSTSIntegration.environment.EnvironmentBean;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author erisidiob
 */
@Configuration
public class VstsServiceConfig {
    
    public final org.apache.logging.log4j.Logger log = LogManager.getLogger(VstsServiceConfig.class);
    public static final String REMOTE_SERVICES_BEAN = "remoteServicesBean";
    public static final String REST_TEMPLATE_BEAN_NAME = "restTemplate";
    
    @Autowired
    public EnvironmentBean environmentBean;
    
    @Bean(value = VstsServiceConfig.REST_TEMPLATE_BEAN_NAME)
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
    
    @Bean(value = REMOTE_SERVICES_BEAN)
    public Map<String, RemoteServiceParameterObject> remoteServices() {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new FileSystemResource(environmentBean.getVSTSRestCallsConfigurationFile());
        Map<String, RemoteServiceParameterObject> remoteServices = new HashMap<>();
        try {
            log.info("remote service call configuration: " + resource.getURL());
            TypeReference type = new TypeReference<Map<String, RemoteServiceParameterObject>>(){};
            remoteServices = mapper.readValue(resource.getURL(), type );
        } catch (IOException ex) {
            Logger.getLogger(VstsServiceConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return remoteServices;
    }

}
