/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maxmara_import.remote;

import java.util.Map;
import lombok.Data;

/**
 *
 * @author erisidiob
 */
@Data
public class RemoteServiceParameterObject {

    private String url;
    private String remoteServiceType;
    private Map<String, String> params;
    
}
