/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author erisidiob
 */
@Data
@AllArgsConstructor
public class User {
    
    private String name;
    private String lastname;
    
}
