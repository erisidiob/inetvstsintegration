/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 *
 * @author erisidiob
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VSTSUser {
    
    private String id;
    private String displayName;
    private String givenName;
    private String jobTitle;
    private String mail;
    private String surname;
    private String userPrincipalName;
    
}
