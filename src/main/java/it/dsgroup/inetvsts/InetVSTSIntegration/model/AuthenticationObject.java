/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author erisidiob
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationObject {
    
    private String access_token;
    private long expires_in;
    private long expires_on;
    private long ext_expires_in;
    private String id_token;
    private long not_before;
    private String refresh_token;
    private String resource;
    private String scope;
    private String token_type;
    
}
