package it.dsgroup.inetvsts.InetVSTSIntegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InetVstsIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(InetVstsIntegrationApplication.class, args);
    }

}
