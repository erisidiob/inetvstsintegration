/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.errorhandling;

import it.dsgroup.inetvsts.InetVSTSIntegration.exception.BaseException;
import org.apache.logging.log4j.LogManager;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author erisidiob
 */
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class DefaultErrorHandler {

    public final org.apache.logging.log4j.Logger log = LogManager.getLogger(DefaultErrorHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(Throwable.class)
    @ResponseBody
    protected void handleExceptionInternal(Throwable ex) {
        log.info("Exception handled by ErrorHandler class");
        log.error("ErrorHandler handleExceptionInternal ex={}", ex);
        HttpStatus status = null;
        String message = "";
        if (ex instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            message = exception.getMessage();
        } else if (ex instanceof BaseException) {
            BaseException exception = (BaseException) ex;
            status = exception.getHttpStatus();
            message = exception.getMessage();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        if (null != status) {
            if (status.is4xxClientError()) {
                log.info("Error calling rest service. Status Code: " + status.value() + ", reason: " + status.getReasonPhrase() + ", message: " + message);
            }
            if (status.is5xxServerError()) {
                log.info("Rest service server error. Status Code: " + status.value() + ", reason: " + status.getReasonPhrase() + ", message: " + message);
            }
        }
    }

}
