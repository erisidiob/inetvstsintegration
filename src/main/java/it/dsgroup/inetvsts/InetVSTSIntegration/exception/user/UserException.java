/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.enel.inetweb.exception.user;

import it.dsgroup.inetvsts.InetVSTSIntegration.exception.BaseException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author erisidiob
 */
public class UserException extends BaseException {

    public static final String USER_NOT_EXISTS = "backend.user.usernotexists";
    public static final String USER_ALREADY_EXISTS = "backend.user.useralreadyexists";
    public static final String USER_CANNOT_SIGNUP = "backend.user.usercannotsignup";
    public static final String USER_WORKER = "backend.user.workeruser";
    public static final String USER_ENDUSER = "backend.user.enduser";
    public static final String USER_OTHER = "backend.user.otheruser";
    private static final long serialVersionUID = 1L;

    //OAUTH2 Error
    public static final String USER_OAUTH2_INVALID_GRANT = "backend.user.oauth2.invalidgrant";

    public static final String USER_OAUTH2_ACCOUNT_STATUS_EXC = "backend.user.oauth2.invalidaccount";
    public static final String USER_OAUTH2_ACCOUNT_STATUS_ERROR = "backend.user.oauth2.accounterror";
    public static final String USER_OAUTH2_CREDENTIAL_NOT_FOUND = "backend.user.oauth2.credentialnotfound";
    public static final String USER_OAUTH2_AUTH_SERVICE = "backend.user.oauth2.authserviceerror";
    public static final String USER_OAUTH2_BAD_CREDENTIALS = "backend.user.oauth2.badcredentials";
    public static final String USER_OAUTH2_REM_AUTH = "backend.user.oauth2.remembermeerror";
    public static final String USER_OAUTH2_USERNAME_NOT_FOUND = "backend.user.oauth2.usernamenotfound";

    //validation
    public static final String USER_USERNAME_EMPTY = "backend.user.usernamenotempty";
    public static final String USER_USERNAME_NULL = "backend.user.usernamenotnull";
    public static final String USER_NAME_EMPTY = "backend.user.namenotempty";
    public static final String USER_NAME_NULL = "backend.user.namenotnull";
    public static final String USER_LASTNAME_EMPTY = "backend.user.lastnamenotempty";
    public static final String USER_LASTNAME_NULL = "backend.user.lastnamenotnull";
    public static final String USER_CONTRACTNUMBER_EMPTY = "backend.user.contractnumbernotempty";
    public static final String USER_CONTRACTNUMBER_NULL = "backend.user.contractnumbernotnull";
    public static final String USER_PASSWORD_EMPTY = "backend.user.passwordnotempty";
    public static final String USER_PASSWORD_NULL = "backend.user.passwordnotnull";
    public static final String USER_USERTYPEID_NULL = "backend.user.usertypeidnotnull";
    public static final String USER_USERTYPECODE_NULL = "backend.user.usertypecodenotnull";
    public static final String USER_USERTYPECODE_EMPTY = "backend.user.usertypecodenotempty";

    public static final String USER_EMAIL_EMPTY = "backend.user.emailnotempty";
    public static final String USER_EMAIL_NULL = "backend.user.emailnotnull";

    public UserException(String errorCode, String description) {
        super(errorCode, description);
    }

    public UserException(String errorCode, HttpStatus httpStatus, String message) {
        super(errorCode, httpStatus, message);

    }

}
