/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.environment;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author erisidiob
 */
@Component
@PropertySource("classpath:environment.properties")
@Data
public class EnvironmentBean {
    
    @Value("${VSTSRestCallsConfigurationFile}")
    private String VSTSRestCallsConfigurationFile;
    
}
