package it.dsgroup.inetvsts.InetVSTSIntegration.controller;

import it.dsgroup.inetvsts.InetVSTSIntegration.service.user.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author erisidiob
 */
@RequestMapping(path = "/rest")
public abstract class BaseController {

    public final org.apache.logging.log4j.Logger log = LogManager.getLogger(BaseController.class);
    
    @Autowired
    private UserServiceImpl userServiceImpl;
    
    public UserServiceImpl getUserServiceImpl(){
        return this.userServiceImpl;
    }
}
