package it.dsgroup.inetvsts.InetVSTSIntegration.controller.user;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.dsgroup.inetvsts.InetVSTSIntegration.controller.BaseController;
import it.dsgroup.inetvsts.InetVSTSIntegration.model.AuthenticationObject;
import it.dsgroup.inetvsts.InetVSTSIntegration.model.User;
import it.dsgroup.inetvsts.InetVSTSIntegration.model.VSTSUser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author erisidiob
 */
@RestController(value = UserController.CONTROLLER_NAME)
public class UserController extends BaseController {

    public static final String CONTROLLER_NAME = "userController";
    public static final String ROOT = "/user/";


    @ApiOperation(value = "Ritorna un messaggio di benvenuto",
            nickname = "welcome", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = String.class)
        ,@ApiResponse(code = 401, message = "Unauthorized")
        ,@ApiResponse(code = 500, message = "Failure", response = String.class)}
    )
    @RequestMapping(method = RequestMethod.GET, path = ROOT + "welcome", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String welcome() {
        return "welcome to user controller class!";
    }

    @ApiOperation(value = "Ritorna un utente in Azure AD",
            nickname = "getAzureADUser", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = VSTSUser.class)
        ,@ApiResponse(code = 401, message = "Unauthorized")
        ,@ApiResponse(code = 500, message = "Failure", response = VSTSUser.class)}
    )
    @RequestMapping(method = RequestMethod.GET, path = ROOT + "current", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public VSTSUser getAzureADUser(@ApiParam(allowEmptyValue = false, required = true, type = "String", name = "access_token", value = "Token dell'utente") @RequestParam(name = "access_token", required = true) String access_token) {
        return getUserServiceImpl().getCurrentUser(access_token);
    }

    
    @ApiOperation(value = "Autentica un utente su Azure AD",
            nickname = "authenticateUser", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = AuthenticationObject.class)
        ,@ApiResponse(code = 401, message = "Unauthorized")
        ,@ApiResponse(code = 500, message = "Failure")}
    )
    @RequestMapping(method = RequestMethod.GET, path = ROOT + "authenticate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public AuthenticationObject authenticateUser(@ApiParam(allowEmptyValue = false, required = true, type = "String", name = "username", value = "Username dell'utente") @RequestParam(name = "username", required = true) String username,
                                                 @ApiParam(allowEmptyValue = false, required = true, type = "String", name = "password", value = "Password dell'utente") @RequestParam(name = "password", required = true) String password) {

        return getUserServiceImpl().authenticateUser(username, password);
    }

}
