/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.inetvsts.InetVSTSIntegration.service;

import com.maxmara_import.remote.RemoteServiceParameterObject;
import it.dsgroup.inetvsts.InetVSTSIntegration.vstsservices.VstsServiceConfig;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author erisidiob
 */
@Component(value = BaseService.COMPONENT_NAME)
public abstract class BaseService {
    
    public static final String COMPONENT_NAME = "baseService";
    public final org.apache.logging.log4j.Logger log = LogManager.getLogger(BaseService.class);
    
    @Autowired
    public RestTemplate restTemplate;
    
    @Autowired
    @Qualifier(value = VstsServiceConfig.REMOTE_SERVICES_BEAN)
    public Map<String, RemoteServiceParameterObject> remoteServicesBean;
    
}
