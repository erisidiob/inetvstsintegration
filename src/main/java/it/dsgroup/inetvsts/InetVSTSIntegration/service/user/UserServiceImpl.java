package it.dsgroup.inetvsts.InetVSTSIntegration.service.user;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import it.dsgroup.inetvsts.InetVSTSIntegration.model.AuthenticationObject;
import it.dsgroup.inetvsts.InetVSTSIntegration.model.VSTSUser;
import it.dsgroup.inetvsts.InetVSTSIntegration.service.BaseService;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

/**
 *
 * @author erisidiob
 */
@Service(value = UserServiceImpl.SERVICE_NAME)
public class UserServiceImpl extends BaseService implements UserService {

    public static final String SERVICE_NAME = "userServiceImpl";

    public String getQuote() {
        log.info("Using rest template for quote...");
        String url = remoteServicesBean.get("getQuoteConfig").getUrl();
        return restTemplate.getForObject(url, String.class);
    }
    
    @Override
    public VSTSUser getCurrentUser(String access_token) {
        log.info("Using rest template for current VSTS user...");
        String url = remoteServicesBean.get("getUserConfig").getUrl();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", "application/json");
        httpHeaders.set("Authorization", "Bearer "+access_token);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        log.info(httpEntity);

        ResponseEntity<VSTSUser> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, VSTSUser.class);
        } catch (RestClientException ex) {
            log.info(ex.getMessage());
            return new VSTSUser();
        }
        
        return ((null == response)? null : response.getBody());
    }

    @Override
    public AuthenticationObject authenticateUser(String username, String password) {
        log.info("Using rest template for authenticate user...");
        String url = remoteServicesBean.get("authenticateUserConfig").getUrl();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<>();
        parametersMap.add("username", username);
        parametersMap.add("password", password);

        if (remoteServicesBean.get("authenticateUserConfig").getRemoteServiceType().equals("POST")) {
            Map<String, String> params = remoteServicesBean.get("authenticateUserConfig").getParams();
            params.keySet().forEach((key) -> {
                parametersMap.add(key, params.get(key));
            });
        }

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(parametersMap, httpHeaders);
        log.info(request);
        //ResponseEntity<AuthenticationObject> response = restTemplate.postForEntity(url, request, AuthenticationObject.class);
        ResponseEntity<AuthenticationObject> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.POST, request, AuthenticationObject.class);
        } catch (RestClientException ex) {
            log.info(ex.getMessage());
            return new AuthenticationObject();
        }
        
        return ((null == response)? null : response.getBody());
    }

}
