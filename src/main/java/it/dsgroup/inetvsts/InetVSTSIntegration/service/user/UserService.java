package it.dsgroup.inetvsts.InetVSTSIntegration.service.user;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import it.dsgroup.inetvsts.InetVSTSIntegration.model.AuthenticationObject;
import it.dsgroup.inetvsts.InetVSTSIntegration.model.VSTSUser;

/**
 *
 * @author erisidiob
 */
public interface UserService {
    
    public VSTSUser getCurrentUser(String access_token);
    public AuthenticationObject authenticateUser(String username, String password);
    
}
